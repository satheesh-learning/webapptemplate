const http = require('http')
var fs = require('fs')

const port = 3000;
const server = http.createServer((req, res) => {

    fs.readFile('./resources/covid_data.txt', function (err, data) {
        if (err) return console.error(err);
        data1 = data.toString();
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Request-Method', '*');
        res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
        res.setHeader('Access-Control-Allow-Headers', '*');

        res.write(data1)
        res.end();

    });

})
function message() {
    console.log("server running")
}
server.listen(port, message)